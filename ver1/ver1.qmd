---
title: Short Paper
subtitle: A Short Subtitle
author:
  - name: Jakub Nowosad
    email: nowosad.jakub@gmail.com
    affiliations: 
        - id: some-tech
          name: Adam Mickiewicz University
          department: Institute of Geoecology and Geoinformation
          address: Krygowskiego 10
          city: Poznan
          postal-code: 61-680
    attributes:
        corresponding: true
abstract: |
  This is the abstract. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum augue turpis, dictum non malesuada a, volutpat eget velit. Nam placerat turpis purus, eu tristique ex tincidunt et. Mauris sed augue eget turpis ultrices tincidunt. Sed et mi in leo porta egestas. Aliquam non laoreet velit. Nunc quis ex vitae eros aliquet auctor nec ac libero. Duis laoreet sapien eu mi luctus, in bibendum leo molestie. Sed hendrerit diam diam, ac dapibus nisl volutpat vitae. Aliquam bibendum varius libero, eu efficitur justo rutrum at. Sed at tempus elit.
keywords: 
  - keyword1
  - keyword2
date: last-modified
bibliography: 2023spquery.bib
format:
  elsevier-pdf:
    keep-tex: true
    journal:
      name: Ecological Informatics
      formatting: preprint
      model: 3p
      cite-style: authoryear
---

<!-- Highlights  -->

# Introduction

@nowosad_motif_2021

# Software

# Case studies

# Discussion

# References {-}
